.PHONY: manual-realops-2019.pdf all clean clean.all

all: manual-realops-2019.pdf

manual-realops-2019.pdf: manual-realops-2019.tex
	latexmk -pdf -xelatex -use-make manual-realops-2019.tex

clean:
	latexmk -c
	find . -type f -name "*.aux" -exec /bin/rm {} \;

clean.all: clean
	latexmk -CA
